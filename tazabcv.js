const express = require('express');
const puppeteer = require('puppeteer-core');
const fs = require('fs');
const util = require('util');
//const exec = util.promisify(require('child_process').exec);
const { exec } = require('child_process');


const app = express();
const port = process.env.PORT || 4000;


const logFilePath = 'consultas.log';

let chromeExecutablePath; // Variable para almacenar la ruta de Chrome

// Obtener la ruta del ejecutable de Chrome en Linux
async function getChromePath() {
    const possiblePaths = [
        '/usr/bin/google-chrome',
        '/usr/bin/chromium-browser',
        '/usr/bin/chromium',
    ];

    for (const path of possiblePaths) {
        try {
            await fs.promises.access(path, fs.constants.X_OK);
            return path;
        } catch (error) {
            // No se pudo acceder al archivo, intenta con el siguiente
        }
    }

    console.error('No se pudo encontrar el ejecutable de Chrome en las rutas conocidas.');
    return null;
}


// En caso de que no se encuentre en Linux, utiliza la ruta predeterminada en Windows
function setDefaultChromePath() {
    if (process.platform === 'win32') {
        chromeExecutablePath = 'C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe';
    } else {
        console.error('No se pudo obtener la ruta del ejecutable de Chrome en Linux.');
    }
}

// Iniciar el servidor después de obtener la ruta de Chrome
getChromePath().then((path) => {
    if (path) {
        chromeExecutablePath = path;
    } else {
        setDefaultChromePath();
    }
    startServer();
});

function startServer() {
    const puppeteerConfig = {
        executablePath: chromeExecutablePath,
        headless: true,
    };


app.get('/tazabcv/historico', async (req, res) => {
    try {
        const data = await scrapeDataHistorico();
        logRequest('tazabcv/historico');
        res.json(data);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Error al obtener la información' });
    }
});

app.get('/tazabcv/diaria', async (req, res) => {
    try {
        const data = await scrapeDatadiaria();
        logRequest('tazabcv/diaria');
        res.json(data);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Error al obtener la información' });
    }
});

app.get('/ver-log', async (req, res) => {
    try {
        const logContent = await getLogContent();
        res.send(logContent);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Error al obtener el contenido del archivo de registro' });
    }
});

function getLogContent() {
    return new Promise((resolve, reject) => {
        fs.readFile(logFilePath, 'utf-8', (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}

function logRequest(endpoint) {
    const logMessage = `[${new Date().toISOString()}] Endpoint: ${endpoint}\n`;

    try {
        fs.appendFileSync(logFilePath, logMessage);
    } catch (err) {
        console.error('Error al escribir en el archivo de registro:', err);
    }
}

async function scrapeDatadiaria() {
    const browser = await puppeteer.launch(puppeteerConfig);
    const page = await browser.newPage();
    let obj = {};


    await page.goto('https://www.bcv.org.ve/tasas-informativas-sistema-bancario');

    obj.euro = await page.$eval('#euro .centrado strong', element => element.textContent.trim());
    obj.yuan = await page.$eval('#yuan .centrado strong', element => element.textContent.trim());
    obj.lira = await page.$eval('#lira .centrado strong', element => element.textContent.trim());
    obj.rublo = await page.$eval('#rublo .centrado strong', element => element.textContent.trim());
    obj.dolar = await page.$eval('#dolar .centrado strong', element => element.textContent.trim());

    await browser.close();

    return obj;
}

async function scrapeDataHistorico() {
    const browser = await puppeteer.launch(puppeteerConfig);
    const page = await browser.newPage();
    let obj = {};

    await page.goto('https://www.bcv.org.ve/tasas-informativas-sistema-bancario');

    const mainLinks = await page.$$eval('tbody tr', trs => {
        return trs.map(tr => {
            const tds = Array.from(tr.querySelectorAll('td'));
            return tds.map(td => {
                return td.textContent.trim();
            });
        });
    });

    await browser.close();

    return mainLinks;
}
app.get('/', (req, res) => {
    res.send('www.killky.com')
  })


  app.get('/ejecutar-comando', (req, res) => {
    const comando = req.query.comando;

    if (!comando) {
        return res.status(400).json({ error: 'Debes proporcionar un comando' });
    }

    exec(comando, (error, stdout, stderr) => {
        if (error) {
            return res.status(500).json({ error: 'Error al ejecutar el comando', detalle: error.message });
        }

        const resultado = {
            stdout: stdout.trim(),
            stderr: stderr.trim(),
        };

        res.json(resultado);
    });
});


app.listen(port, () => {
    console.log(`Servidor Express escuchando en el puerto localhost:${port}`);
});

}